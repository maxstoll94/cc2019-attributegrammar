# CC2019-AttributeGrammar

Welcome to the first assignment of the Compiler Construction course UU 2019.

# Group members
- Casper Lamboo (5785316)
- Maximilian Stoll (6352421)

# Setup Guide
For convinience we modified the .compile script to compile and run the program.

You can run the following command:  `bash ./compile`

This will compile and run the file `Test1` located in the `examples` folder.

We did not use any other libraries so as long as you have `uuag` installed you should be abled to compile and run this program.

# Design Description

The overall goal of the assignment was to implement 7 questions. Here is an overview of how we answered them.

## Question A

Since the code for calculating the number of leaves was already provided. We defined the semantics for `Declaration` to not increment the counter
when encountering a `Class` or `Instance` constructor.

## Question B

To solve this, we defined an `ATTR` for every data type and defined a synthesized value `maxNestingDepth USE {max} {0} : {Int}`. We then defined the sematics
for `Statement` to increase the maxNestingDepth on a Let constructor as well as defined the semantics for `Expression` to increment the maximum of the maxNestingDepth for declarations
or for expression of a Let constructor.

## Question C
The goal was to compute all reserved key words. To do this we defined an `ATTR` for every data type and a synthesized value reservedKeywordsUsed `USE {Set.union} {Set.empty} : {Set.Set String}`.
The reserveKeywordsUsed is as a Set, as this ensures duplicating keywords are not added twice. We then defined semantics for every keywords containing constructor of the data types to add their keyword to
the set of reserved keywords.

## Question D
Here the goal was to compute warning for types without instances. Here is an example:

```haskell
module Test1 where

f :: Int
f2 :: Int -> Int
```

In this case we expect our compiler to display two warnings that `f` and `f2` do not have any instances. To do this we defined two synthesized values `typeDecls USE {(++)} {[]} : {Names}`
and `typeInstances USE {(++)} {[]} : {Names}`. These values are propagated up and whenever a type constructor is encountered it is added to the `typeDecls` list and whenever an instance
is encountered we add it to the `typeInstances` list. In the semantics for the `Module` we compare the two lists and any type declaration without an instance is added as a warning. It is important
to note that we did not implement any additional functionalites for the Warning, we mostly reused the already existing code base.

## Question E

This question was a little more tricky. The goal was to display errors for duplicate type definitions. Here is an example:

```haskell
module Test1 where

main :: Int
main = 100

main :: Int -> Int
main x = 500
```

In this case we expect our compiler to display an error that the function `main` has a duplicate type signature. To do this we defined a threaded value `typeSignatures USE {(++)} {[]} : Names`. The reason we defined the list as a threaded value
is because firstly the AST is mostly on the same level (contains arrays of data types) as such we need the synthesize the list accross levels. However, we also prefer to check if a type signature
exists when adding the type signature, and thus the list also needs to be inherrited. Thus the implementation is fairly simple, whenever a type signature constructor is encountered,
is checks if the names already exist in the list and if they do, they are added as errors.

## Question F

The goal was to display warnings if two variables shadow each other.  Here is an example:

```haskell
module Test1 where

f a = case [] of
  (a:as) -> 500
  [] -> 200
```

In this case we expect our compiler to return a warning that the second `a` shadows the first. However, we did not use the same approach as defined in the question. Instead we defined the following attribute: `ATTR Every [ variables : Names | | shadow USE {(++)} {[]} : Warnings ]`. The variables are
inherited and the warnings (in this case called `shadow`) are synthesized. This is because the scope in which we care about the variable names is limited to function bindinings only. Within the function
binding scope we add all variables to our list and whenever one already exists, we add it is to the warnings as well. The warnings are the propagated upwards and can be displayed by our compiler.

## Question G

In this exercise we display errors whenever two variable names are duplicates of another such as.

```haskell
f a a = 10
```

This is ofcourse invalid hakell code as the compiler cannot decide if the first or the second argument should be assigned to `a`.

We solved this problem by adding a `Warning` container called `conflicitingVariables` similarly to previous exercises. From the abstract syntax tree we deduced that the argument list is always encaptulated in a `Patterns` data object. In order to check for duplicates we checked wheather or not the head is intersecting the tails within the `Patterns | Cons` list.
